package com.example1.demo1.payload.OvertimeDetail;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Builder
public class OvertimeDetailResponse {

    private Long id;

    private String fullname;

    private String teamName;

    private String departmentName;

    private String position;

    private String firstManagerName;

    private String secondManagerName;

    private String overtimeDate;

    private Long dayOfWeek;

    private String dayOff;

    private String startTime;

    private String endTime;

    private Double totalHours;

    private String objective;

    private String status;

}
