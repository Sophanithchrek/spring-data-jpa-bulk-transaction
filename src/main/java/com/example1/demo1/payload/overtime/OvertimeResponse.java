package com.example1.demo1.payload.overtime;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Builder
public class OvertimeResponse {

    private UUID id;

    private String description;

    private String reportFor;

    private String startDate;

    private String endDate;

    private String createdOn;

    private String status;

}
