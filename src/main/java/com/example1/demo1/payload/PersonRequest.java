package com.example1.demo1.payload;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PersonRequest {

    private String name;

    private String startWorkTime;

    private String endWorkTime;

}
