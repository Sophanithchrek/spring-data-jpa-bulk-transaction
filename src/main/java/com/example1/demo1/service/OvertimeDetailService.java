package com.example1.demo1.service;

import com.example1.demo1.model.Overtime;
import com.example1.demo1.model.OvertimeDetail;
import com.example1.demo1.payload.OvertimeDetail.OvertimeDetailRequest;
import com.example1.demo1.payload.OvertimeDetail.OvertimeDetailResponse;
import com.example1.demo1.repository.OvertimeDetailRepository;
import com.example1.demo1.repository.OvertimeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OvertimeDetailService {

    @Autowired
    private OvertimeRepository overtimeRepository;

    @Autowired
    private OvertimeDetailRepository overtimeDetailRepository;

    public OvertimeDetail insert(OvertimeDetailRequest overtimeDetailRequest) {
        Optional<Overtime> ot = overtimeRepository.findById(overtimeDetailRequest.getOvertimeId());
        OvertimeDetail otd = new OvertimeDetail(overtimeDetailRequest, ot.get());
        return overtimeDetailRepository.save(otd);
    }

    public List<OvertimeDetail> insertAll(List<OvertimeDetailRequest> overtimeDetailList) {

        Optional<Overtime> ot = overtimeRepository.findById(overtimeDetailList.get(0).getOvertimeId());

        List<OvertimeDetail> otd = overtimeDetailList.stream().map(x -> {

            return OvertimeDetail.builder()
                    .id(x.getId())
                    .dayOfWeek(x.getDayOfWeek())
                    .dayOff(x.getDayOff())
                    .departmentName(x.getDepartmentName())
                    .endTime(LocalTime.parse(x.getEndTime()))
                    .firstManagerName(x.getFirstManagerName())
                    .fullname(x.getFullname())
                    .objective(x.getObjective())
                    .overtimeDate(LocalDate.parse(x.getOvertimeDate(), DateTimeFormatter.ISO_LOCAL_DATE))
                    .position(x.getPosition())
                    .secondManagerName(x.getSecondManagerName())
                    .startTime(LocalTime.parse(x.getStartTime()))
                    .status(x.getStatus())
                    .teamName(x.getTeamName())
                    .totalHours(x.getTotalHours())
                    .overtime(ot.get())
                    .build();

        }).collect(Collectors.toList());

        return overtimeDetailRepository.saveAll(otd);
    }

    public List<OvertimeDetail> getAllOvertimeDetailByOvertimeId(UUID overtimeId) {
        return overtimeDetailRepository.findAllByOvertimeId(overtimeId);
    }


    public boolean update(OvertimeDetailRequest overtimeDetailRequest) {
        try {

            Optional<Overtime> ot = overtimeRepository.findById(overtimeDetailRequest.getOvertimeId());

            OvertimeDetail otd = new OvertimeDetail(overtimeDetailRequest, ot.get());

            overtimeDetailRepository.save(otd);
            return true;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public boolean updateAll(List<OvertimeDetailRequest> overtimeDetailRequestList) {
        try {

            Optional<Overtime> ot = overtimeRepository.findById(overtimeDetailRequestList.get(0).getOvertimeId());

            List<OvertimeDetail> overtimeDetails = overtimeDetailRequestList.stream().map(x -> {

                return OvertimeDetail.builder()
                        .id(x.getId())
                        .dayOfWeek(x.getDayOfWeek())
                        .dayOff(x.getDayOff())
                        .departmentName(x.getDepartmentName())
                        .endTime(LocalTime.parse(x.getEndTime()))
                        .firstManagerName(x.getFirstManagerName())
                        .fullname(x.getFullname())
                        .objective(x.getObjective())
                        .overtimeDate(LocalDate.parse(x.getOvertimeDate(), DateTimeFormatter.ISO_LOCAL_DATE))
                        .position(x.getPosition())
                        .secondManagerName(x.getSecondManagerName())
                        .startTime(LocalTime.parse(x.getStartTime()))
                        .status(x.getStatus())
                        .teamName(x.getTeamName())
                        .totalHours(x.getTotalHours())
                        .overtime(ot.get())
                        .build();

            }).collect(Collectors.toList());

            overtimeDetailRepository.saveAll(overtimeDetails);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public List<OvertimeDetailResponse> findAllOvertimeDetails() {

        List<OvertimeDetail> overtimeDetailsList = overtimeDetailRepository.findAllOvertimeDetails();

        List<OvertimeDetailResponse> response = overtimeDetailsList.stream().map(x -> {
            return OvertimeDetailResponse.builder()
                    .id(x.getId())
                    .dayOfWeek(x.getDayOfWeek())
                    .dayOff(x.getDayOff())
                    .departmentName(x.getDepartmentName())
                    .endTime(x.getEndTime().format(DateTimeFormatter.ofPattern("HH:mm:ss")))
                    .firstManagerName(x.getFirstManagerName())
                    .fullname(x.getFullname())
                    .objective(x.getObjective())
                    .overtimeDate(x.getOvertimeDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")))
                    .position(x.getPosition())
                    .secondManagerName(x.getSecondManagerName())
                    .startTime(x.getStartTime().format(DateTimeFormatter.ofPattern("HH:mm:ss")))
                    .status(x.getStatus())
                    .teamName(x.getTeamName())
                    .totalHours(x.getTotalHours())
                    .build();
        }).collect(Collectors.toList());

        return response;
    }

}
