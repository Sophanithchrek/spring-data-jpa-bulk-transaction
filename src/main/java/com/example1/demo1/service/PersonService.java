package com.example1.demo1.service;

import com.example1.demo1.model.Person;
import com.example1.demo1.payload.PersonRequest;
import com.example1.demo1.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public Person insertPerson(PersonRequest personRequest) {
        Person person = new Person(personRequest);
        return personRepository.save(person);
    }

    public boolean updateAllPeople(List<Person> p) {
        try {
            personRepository.saveAll(p);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
