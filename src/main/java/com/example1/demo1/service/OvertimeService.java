package com.example1.demo1.service;

import com.example1.demo1.exception.BusinessException;
import com.example1.demo1.exception.ErrorType.ErrorType;
import com.example1.demo1.model.Overtime;
import com.example1.demo1.payload.overtime.OvertimeResponse;
import com.example1.demo1.repository.OvertimeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class OvertimeService {

    @Autowired
    private OvertimeRepository overtimeRepository;

    // get
    public String test() {
        return "hello";
    }

    // get all
    public List<OvertimeResponse> findAllOvertime() {

        List<Overtime> overtimeList = overtimeRepository.findAllOvertime();

        List<OvertimeResponse> response = overtimeList.stream().map(x -> {
            return OvertimeResponse.builder()
                    .id(x.getId())
                    .description(x.getDescription())
                    .reportFor(x.getReportFor())
                    .startDate(x.getStartDate().format(DateTimeFormatter.ofPattern("dd-MMMM-yyyy")))
                    .endDate(x.getEndDate().format(DateTimeFormatter.ofPattern("dd-MMMM-yyyy")))
                    .createdOn(x.getCreatedOn().format(DateTimeFormatter.ofPattern("dd-MMMM-yyyy HH:mm:ss")))
                    .status(x.getStatus())
                    .build();
        }).collect(Collectors.toList());

        return response;
    }


    // save
    public Overtime insertOvertime(Overtime ot) {
        return overtimeRepository.save(ot);
    }


    // save all
    public List<Overtime> insertAllOvertime(List<Overtime> overtimeList) {
        return overtimeRepository.saveAll(overtimeList);
    }

    // update
    public Overtime updateOvertime(Overtime overtime) throws BusinessException {
        Overtime ot = overtimeRepository.findById(overtime.getId()).orElseThrow(() -> new BusinessException(ErrorType.OVERTIME_NOT_FOUND));
        return overtimeRepository.save(overtime);
    }

    // update all
    public boolean updateAllOvertime(List<Overtime> overtimeList) {
        try {
            overtimeRepository.saveAll(overtimeList);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    // remove by id
    public void deleteOvertime(UUID overtimeId) throws BusinessException {
        Overtime overtime = overtimeRepository.findById(overtimeId).orElseThrow(() -> new BusinessException(ErrorType.OVERTIME_NOT_FOUND));
        overtime.setStatus("9");
        overtimeRepository.save(overtime);
    }
}
