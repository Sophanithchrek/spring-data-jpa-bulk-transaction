package com.example1.demo1.utils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class TimezoneUtils {

    private static String timeZone() {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"), Locale.getDefault());
        String   timeZone = new SimpleDateFormat("Z").format(calendar.getTime());
        return timeZone.substring(0, 3);
    }

    public static String getTimezone() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        return timestamp+timeZone();
    }

}
