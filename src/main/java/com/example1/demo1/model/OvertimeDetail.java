package com.example1.demo1.model;

import com.example1.demo1.payload.OvertimeDetail.OvertimeDetailRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "overtime_details")
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class OvertimeDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    private String fullname;

    private String teamName;

    private String departmentName;

    private String position;

    private String firstManagerName;

    private String secondManagerName;

    private LocalDate overtimeDate;

    private Long dayOfWeek;

    private String dayOff;

    private LocalTime startTime;

    private LocalTime endTime;

    @Column(columnDefinition = "decimal(10,2)")
    private Double totalHours;

    private String objective;

    @JsonIgnore
    private String status = "1";

    @ManyToOne(cascade = CascadeType.DETACH, optional = false)
    @JoinColumn(name = "overtime_id",referencedColumnName = "id", nullable = false)
    private Overtime overtime;

    public OvertimeDetail(OvertimeDetailRequest payload, Overtime overtime) {
        this.id = payload.getId();
        this.fullname = payload.getFullname();
        this.teamName = payload.getTeamName();
        this.departmentName = payload.getDepartmentName();
        this.position = payload.getPosition();
        this.firstManagerName = payload.getFirstManagerName();
        this.secondManagerName = payload.getSecondManagerName();
        this.dayOff = payload.getDayOff();
        this.dayOfWeek = payload.getDayOfWeek();
        this.overtimeDate = LocalDate.parse(payload.getOvertimeDate(), DateTimeFormatter.ISO_LOCAL_DATE);
        this.startTime = LocalTime.parse(payload.getStartTime());
        this.endTime =  LocalTime.parse(payload.getEndTime());
        this.totalHours = payload.getTotalHours();
        this.objective = payload.getObjective();
        this.overtime = overtime;
    }

}

