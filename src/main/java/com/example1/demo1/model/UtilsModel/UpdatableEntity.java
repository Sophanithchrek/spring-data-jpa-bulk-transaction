package com.example1.demo1.model.UtilsModel;

import lombok.*;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.*;

import javax.persistence.*;
import java.time.*;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Data
@EqualsAndHashCode(callSuper=false)
public abstract class UpdatableEntity {

    @LastModifiedDate
    private ZonedDateTime updatedOn;

}

