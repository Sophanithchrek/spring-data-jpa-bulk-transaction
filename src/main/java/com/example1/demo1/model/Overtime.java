package com.example1.demo1.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "overtimes")
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Overtime {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(length = 100)
    private String description;

    @Column(length = 2)
    private String reportFor;

    private LocalDate startDate;

    private LocalDate endDate;

    @JsonIgnore
    private LocalDateTime createdOn = LocalDateTime.now();

    @JsonIgnore
    private String status = "1";

}
