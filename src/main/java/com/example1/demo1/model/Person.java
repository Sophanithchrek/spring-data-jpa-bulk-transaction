package com.example1.demo1.model;

import com.example1.demo1.payload.PersonRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.*;

import javax.persistence.*;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "persons")
@Builder
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class Person {

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    private LocalTime startWorkTime;

    private LocalTime endWorkTime;

    @JsonIgnore
    private String status = "1";

    public Person(PersonRequest personRequest) {
        this.name = personRequest.getName();
        this.setStartWorkTime(LocalTime.parse(personRequest.getStartWorkTime()));
        this.setEndWorkTime(LocalTime.parse(personRequest.getEndWorkTime()));
    }

}
