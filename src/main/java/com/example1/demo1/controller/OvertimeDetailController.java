package com.example1.demo1.controller;

import com.example1.demo1.model.OvertimeDetail;
import com.example1.demo1.payload.OvertimeDetail.OvertimeDetailRequest;
import com.example1.demo1.service.OvertimeDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class OvertimeDetailController {

    @Autowired
    private OvertimeDetailService overtimeDetailService;

    // get all
    @GetMapping("/overtime-details")
    public ResponseEntity<?> getAllOvertimeDetails() {
        return ResponseEntity.ok(overtimeDetailService.findAllOvertimeDetails());
    }


    @PostMapping("/overtime-detail")
    public String addOvertimeDetail(@RequestBody OvertimeDetailRequest overtimeDetail) {
        if(overtimeDetail != null) {
            overtimeDetailService.insert(overtimeDetail);
            return "Added a overtimeDetail";
        } else {
            return "REQUEST_NO_BODY";
        }
    }


    @PostMapping("/overtime-detail-all")
    public String addAllOvertimeDetail(@RequestBody List<OvertimeDetailRequest> overtimeDetailList) {
        if(overtimeDetailList != null && !overtimeDetailList.isEmpty()) {
            overtimeDetailService.insertAll(overtimeDetailList);
            return String.format("Added %d overtime detail.", overtimeDetailList.size());
        } else {
            return "REQUEST_NO_BODY";
        }
    }


    @GetMapping("/overtime-detail")
    public List<OvertimeDetail> getAllOvertimeDetailByOvertimeId(@RequestParam("overtimeId") UUID overtimeId) {
        return overtimeDetailService.getAllOvertimeDetailByOvertimeId(overtimeId);
    }


    @PutMapping("/overtime-detail")
    public String updateOvertimeDetail(@RequestBody OvertimeDetailRequest overtimeDetail) {
        if(overtimeDetail != null) {
            overtimeDetailService.update(overtimeDetail);
            return "Updated overtimeDetail.";
        } else {
            return "REQUEST_NO_BODY";
        }
    }


    @PutMapping("/overtime-detail-all")
    public String updatePeople(@RequestBody List<OvertimeDetailRequest> overtimeDetailList) {
        if(overtimeDetailList != null) {
            overtimeDetailService.updateAll(overtimeDetailList);
            return "Updated all overtime detail.";
        } else {
            return "REQUEST_NO_BODY";
        }
    }


}
