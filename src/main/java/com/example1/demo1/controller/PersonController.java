package com.example1.demo1.controller;

import com.example1.demo1.model.Person;
import com.example1.demo1.payload.PersonRequest;
import com.example1.demo1.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class PersonController {

    @Autowired
    private PersonService personService;

    @PostMapping("/person")
    public String addPerson(@RequestBody PersonRequest person) {
        if(person != null) {
            personService.insertPerson(person);
            return "Added a person";
        } else {
            return "REQUEST_NO_BODY";
        }
    }


    @PutMapping("bulk")
    public String updateAllPeople(@RequestBody List<Person> people) {
        if(people != null) {
            personService.updateAllPeople(people);
            return "Updated all people.";
        } else {
            return "REQUEST_NO_BODY";
        }
    }

}
