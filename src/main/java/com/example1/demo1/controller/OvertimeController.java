package com.example1.demo1.controller;

import com.example1.demo1.exception.BusinessException;
import com.example1.demo1.exception.ErrorType.ErrorType;
import com.example1.demo1.model.Overtime;
import com.example1.demo1.payload.overtime.OvertimeResponse;
import com.example1.demo1.service.OvertimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class OvertimeController {

    @Autowired
    private OvertimeService overtimeService;

    @GetMapping("/test")
    public ResponseEntity<?> test() {
        return ResponseEntity.ok(overtimeService.test());
    }

    @GetMapping("/overtime")
    public List<OvertimeResponse> getAllOvertime() {
        return overtimeService.findAllOvertime();
    }

    @PostMapping("/overtime")
    public ResponseEntity<?> addOvertime(@RequestBody Overtime overtime) {
        overtimeService.insertOvertime(overtime);
        return ResponseEntity.ok("Overtime is saved");
    }

    @PostMapping("/overtime-list")
    public ResponseEntity<?> addAllOvertime(@RequestBody List<Overtime> overtimeList) {
        if(overtimeList != null && !overtimeList.isEmpty()) {
            overtimeService.insertAllOvertime(overtimeList);
            return ResponseEntity.ok("Overtime list is saved");
        } else {
            return ResponseEntity.ok("Request to save is failed");
        }
    }

    @PutMapping("overtime-one")
    public ResponseEntity<?> updateOvertime(@RequestBody Overtime overtime) throws BusinessException {
        return ResponseEntity.ok(overtimeService.updateOvertime(overtime));
    }


    @PutMapping("/overtime")
    public String updateAllOvertime(@RequestBody List<Overtime> overtimeList) {
        if(overtimeList != null) {
            overtimeService.updateAllOvertime(overtimeList);
            return "Updated all overtime.";
        } else {
            return "REQUEST_NO_BODY";
        }
    }


    @DeleteMapping("/overtime/{id}")
    public ResponseEntity<?> deleteOvertime(@PathVariable("id") UUID overtimeId) throws BusinessException {
        overtimeService.deleteOvertime(overtimeId);
        return ResponseEntity.ok("Overtime is removed");
    }

}
