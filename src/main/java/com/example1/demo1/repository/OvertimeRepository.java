package com.example1.demo1.repository;

import com.example1.demo1.model.Overtime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface OvertimeRepository extends JpaRepository<Overtime, UUID> {

    @Query(nativeQuery = true, value = "SELECT * FROM overtimes WHERE status = '1' ORDER BY created_on DESC")
    List<Overtime> findAllOvertime();

}
