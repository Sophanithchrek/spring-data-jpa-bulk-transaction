package com.example1.demo1.repository;

import com.example1.demo1.model.OvertimeDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface OvertimeDetailRepository extends JpaRepository<OvertimeDetail, Long> {

    @Query(nativeQuery = true, value = "SELECT * FROM overtime_details WHERE overtime_id = :overtimeId AND status = '1' ORDER BY id DESC")
    List<OvertimeDetail> findAllByOvertimeId(UUID overtimeId);

    @Query(nativeQuery = true, value = "SELECT * FROM overtime_details ORDER BY id DESC")
    List<OvertimeDetail> findAllOvertimeDetails();

}
