package com.example1.demo1.exception;

import com.example1.demo1.exception.ErrorType.ErrorType;

public class BusinessException extends Exception {

    private ErrorType errorCode;

    public BusinessException(ErrorType errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    public BusinessException(ErrorType errorCode) {
        super(errorCode.getMessage());
        this.errorCode = errorCode;
    }

    public BusinessException(ErrorType errorCode, Throwable e) {
        this(errorCode);
        System.out.println(e);
    }

    public ErrorType getErrorCode() {
        return errorCode;
    }

}
